//
//  Fixtures.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 05/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import Foundation

class Fixtures{
    
    
    private var home: String?
    private var away: String?
    private var score: String?
    private var matchDay: Int?
    

    public func getHomeTeamName()->String{
        return home!
    }
    public func getAwayTeamName()->String{
        return away!
    }
    public func getScore()->String{
        return score!
    }
    public func getMatchDay()->Int{
        return matchDay!
    }
    
    init(home: String, away: String, score: String, matchDay: Int) {
        self.home = home
        self.away = away
        self.score = score
        self.matchDay = matchDay
    }

}
