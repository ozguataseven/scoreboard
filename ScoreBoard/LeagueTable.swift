//
//  LeagueTable.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 29/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import Foundation

    class LeagueTable{
        private var position : Int?
        private var teamName : String?
        private var playedGames : Int?
        private var points : Int?
        private var goals : Int?
        private var goalsAgainst : Int?
        private var goalDifference : Int?
        private var wins : Int?
        private var draws : Int?
        private var losses : Int?
        
        
        public func getPosition()->Int{
            return position!
        }
        public func getTeamName()->String{
            return teamName!
        }
        public func getPlayedGames()->Int{
            return playedGames!
        }
        public func getPoints()->Int{
            return points!
        }
        public func getGoals()->Int{
            return goals!
        }
        public func getGoalsAgainst()->Int{
            return goalsAgainst!
        }
        public func getGoalDifference()->Int{
            return goalDifference!
        }
        public func getWins()->Int{
            return wins!
        }
        public func getDraws()->Int{
            return draws!
        }
        public func getLosses()->Int{
            return losses!
        }
        
        
        init(position : Int,
            teamName : String,
            playedGames : Int,
            points : Int,
            goals : Int,
            goalsAgainst : Int,
            goalDifference : Int,
            wins : Int,
            draws : Int,
            losses : Int) {
            
            self.position = position
            self.teamName = teamName
            self.playedGames = playedGames
            self.points = points
            self.goals = goals
            self.goalsAgainst = goalsAgainst
            self.goalDifference = goalDifference
            self.wins = wins
            self.draws = draws
            self.losses = losses
            
        }

}
