//
//  TeamsViewController.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 27/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit
import Alamofire

class TeamsViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet var teamsTableView: UITableView!
    
    var taggedBtn = Int()
 
    var teamNames = [String]()
    
    var teamPlayerLinks = [String] ()
    
    var teamsLeagueCode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("http://api.football-data.org/v1/competitions/" + teamsLeagueCode + "/teams").responseJSON { (response) in
            if let JSON = response.result.value as? [String : Any]{
                
                if let teamsFromJson = JSON["teams"] as? [[String : AnyObject]] {
                    
                    for teamsFromJson in teamsFromJson{
                        
                        let teamName = teamsFromJson["name"] as! String
                        
                        let links = teamsFromJson["_links"] as? [String : AnyObject]
                        
                        let players = links?["players"] as? [String : AnyObject]
                        
                        let href = players?["href"] as! String
                        
                        self.teamNames.append(teamName)
                        self.teamPlayerLinks.append(href)
                        
                        self.teamsTableView.reloadData()
                        
                        
                    }
                }
            }
            
            
            
        }

        // Do any additional setup after loading the view.
        teamsTableView.register(UINib(nibName: "TeamsTableCell",bundle: nil), forCellReuseIdentifier: "TeamsTableCell")
        
        teamsTableView.allowsSelection = false
        teamsTableView.separatorStyle = .none
        view.addSubview(teamsTableView)
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = teamsTableView.dequeueReusableCell(withIdentifier: "TeamsTableCell", for: indexPath) as! TeamsTableCell
        cell.teamName.text = teamNames[indexPath.row]
        
        if indexPath.row % 2 == 0
        {
            cell.contentView.backgroundColor = UIColor(red: 220/255, green: 200/255, blue: 220/255, alpha: 1)
            
        }
        else
        {
            cell.contentView.backgroundColor  = UIColor (red: 220/255, green: 222/255, blue: 226/255, alpha: 1)
        }
        cell.teamInfoBtn.tag = indexPath.row
        cell.teamInfoBtn.addTarget(self, action: #selector(TeamsViewController.someAction), for: .touchUpInside)

        return cell
    }
    
    func someAction(_ sender: AnyObject){
        taggedBtn = sender.tag
        self.performSegue(withIdentifier: "playerDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let data = segue.destination as! PlayerDetailViewController
        
        data.teamName = teamNames[taggedBtn]
        data.teamLink = teamPlayerLinks[taggedBtn]
     
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamNames.count
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
