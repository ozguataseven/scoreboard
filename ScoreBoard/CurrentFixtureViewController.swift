//
//  ViewController.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 05/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit
import Alamofire


class CurrentFixtureViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    var myTableView: UITableView!
   
    var fixtures = [Fixtures]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Alamofire.request("http://api.football-data.org/v1/competitions/424/fixtures").responseJSON { (response) in
            if let JSON = response.result.value as? [String : Any]{
  
                if let fixturesFromJson = JSON["fixtures"] as? [[String : AnyObject]] {
                    
                    for fixturesFromJson in fixturesFromJson{
                        
                        let homeTeamName = fixturesFromJson["homeTeamName"] as! String
                        let awayTeamName = fixturesFromJson["awayTeamName"] as! String
                        let matchDay = fixturesFromJson["matchday"] as! Int
                        
                        let result = fixturesFromJson["result"] as? [String : AnyObject]
                        
                        let goalsHomeTeam = result?["goalsHomeTeam"] as! Int
                        let goalsAwayTeam = result?["goalsAwayTeam"] as! Int
                        
                        let scr = String(goalsHomeTeam) + " - " + String(goalsAwayTeam)
                        
                        self.fixtures.append(Fixtures(home: homeTeamName, away: awayTeamName, score: scr ,matchDay: matchDay))
                        
                        self.myTableView.reloadData()
                        
                        
                    }
                }
            }
                    
       
        
        }
        myTableView = UITableView(frame: view.frame)
        myTableView.layer.frame.size.height = view.frame.height * 1.5  //????
        myTableView.frame.origin.y += 50
        myTableView.register(UINib(nibName: "ScoreTableCell",bundle: nil), forCellReuseIdentifier: "ScoreTableCell")
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.allowsSelection = false
        myTableView.separatorStyle = .none
        myTableView.backgroundColor = UIColor(red: 81/255, green: 104/255, blue: 130/255, alpha: 1)
        view.addSubview(myTableView)
        view.backgroundColor = UIColor(red: 81/255, green: 104/255, blue: 130/255, alpha: 1)
      
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fixtures.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myTableView.dequeueReusableCell(withIdentifier: "ScoreTableCell", for: indexPath) as! ScoreTableCell
        cell.homeTeamLabel.text = fixtures[indexPath.row].getHomeTeamName()
        cell.awayTeamLabel.text = fixtures[indexPath.row].getAwayTeamName()
        cell.score.text = fixtures[indexPath.row].getScore()
        cell.xibView.backgroundColor = UIColor(red: 81/255, green: 104/255, blue: 130/255, alpha: 1)
        
        if indexPath.row % 2 == 0
        {
            cell.contentView.backgroundColor = UIColor(red: 220/255, green: 200/255, blue: 220/255, alpha: 1)
            
        }
        else
        {
            cell.contentView.backgroundColor  = UIColor (red: 220/255, green: 222/255, blue: 226/255, alpha: 1)
        }
              
        return cell
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

