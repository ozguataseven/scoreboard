//
//  PlayerDetailViewController.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 28/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit
import Alamofire

class PlayerDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var playerInfosTableView: UITableView!
    
    @IBOutlet var teamNameLAbel: UILabel!
    
    var teamName = String()
    
    var teamLink = String()
    
    var players = [Players]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Alamofire.request(teamLink).responseJSON { (response) in
            if let JSON = response.result.value as? [String : Any]{
                
                if let playersFromJson = JSON["players"] as? [[String : AnyObject]] {
                    
                    for playersFromJson in playersFromJson{
                        
                        var name = playersFromJson["name"] as? String
                        if name == nil
                        {
                            name = " "
                        }
                        var position = playersFromJson["position"] as? String
                        if position == nil
                        {
                            position = " "
                        }
                        var jerseyNumber = playersFromJson["jerseyNumber"] as? Int
                        if jerseyNumber == nil
                        {
                            jerseyNumber = 0
                        }
                        var dateOfBirth = playersFromJson["dateOfBirth"] as? String
                        if dateOfBirth == nil
                        {
                            dateOfBirth = " "
                        }
                        var nationality = playersFromJson["nationality"] as? String
                        if nationality == nil
                        {
                            nationality = " "
                        }
                        var contractUntil = playersFromJson["contractUntil"] as? String
                        if contractUntil == nil
                        {
                            contractUntil = " "
                        }
                        
                        self.players.append(Players(name: name!, position: position!, jerseyNumber: jerseyNumber!, dateOfBirth: dateOfBirth!, nationality: nationality!, contractUntil: contractUntil!))
        
                        self.playerInfosTableView.reloadData()
                        
                        
                    }
                }
            }
            
            
            
        }
        
        teamNameLAbel.text = teamName
        playerInfosTableView.register(UINib(nibName: "PlayerTableCell",bundle: nil), forCellReuseIdentifier: "PlayerTableCell")
        playerInfosTableView.allowsSelection = false
        playerInfosTableView.separatorStyle = .none
        view.addSubview(playerInfosTableView)
  
        
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = playerInfosTableView.dequeueReusableCell(withIdentifier: "PlayerTableCell", for: indexPath) as! PlayerTableCell
        cell.playerLabel.text = players[indexPath.row].getName()
        cell.kitNo.text = String(players[indexPath.row].getJerseyNumber())
        cell.dateOfBirthLabel.text = players[indexPath.row].getDateOfBirth()
        cell.nationalityLabel.text = players[indexPath.row].getNationality()
        cell.contractUntilLabel.text = players[indexPath.row].getContractUntil()
        cell.playerProfile.image = UIImage(named: "football-player")
        
        switch players[indexPath.row].getPosition() {
        case "Keeper":
            cell.playerPosition.image = UIImage(named: "GK")
        case "Centre-Back":
            cell.playerPosition.image = UIImage(named: "CB")
        case "Left-Back":
            cell.playerPosition.image = UIImage(named: "LB")
        case "Right-Back":
            cell.playerPosition.image = UIImage(named: "RB")
        case "Defensive Midfield":
            cell.playerPosition.image = UIImage(named: "DM")
        case "Central Midfield":
            cell.playerPosition.image = UIImage(named: "CM")
        case "Right Midfield":
            cell.playerPosition.image = UIImage(named: "RM")
        case "Left Midfield":
            cell.playerPosition.image = UIImage(named: "LM")
        case "Attacking Midfield":
            cell.playerPosition.image = UIImage(named: "AM")
        case "Left Wing":
            cell.playerPosition.image = UIImage(named: "LW")
        case "Right Wing":
            cell.playerPosition.image = UIImage(named: "RW")
        case "Centre-Forward":
            cell.playerPosition.image = UIImage(named: "CF")
  
        default:
            print("nothing")
        }
        
        if indexPath.row % 2 == 0
        {
            cell.contentView.backgroundColor = UIColor(red: 220/255, green: 200/255, blue: 220/255, alpha: 1)
           
        }
        else
        {
            cell.contentView.backgroundColor  = UIColor (red: 220/255, green: 222/255, blue: 226/255, alpha: 1)
        }

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 146
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
