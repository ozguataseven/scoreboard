//
//  StandingViewController.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 29/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit
import Alamofire

class StandingViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource{

    @IBOutlet var standingTableView: UITableView!
    
    @IBOutlet var label1: UILabel!
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    
    @IBOutlet var label4: UILabel!
    
    var teamsLeagueCode = String()
    
    var standings = [LeagueTable]()
  
    var isPrem : Bool = false
    
    var isBra : Bool = false
    
    var isFre : Bool = false
    
    var isNet : Bool = false
    
    var isGer : Bool = false
    
    var cellColor : UIColor = UIColor.white
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Alamofire.request("http://api.football-data.org/v1/competitions/" + teamsLeagueCode + "/leagueTable").responseJSON { (response) in
            if let JSON = response.result.value as? [String : Any]{
                
                if let standingsFromJson = JSON["standing"] as? [[String : AnyObject]] {
                    
                    for standingsFromJson in standingsFromJson{
                        
                        
                        let position = standingsFromJson["position"] as! Int
                        
                        let teamName = standingsFromJson["teamName"] as! String
                        
                        let playedGames = standingsFromJson["playedGames"] as! Int
                        
                        let points = standingsFromJson["points"] as! Int
                        
                        let goals = standingsFromJson["goals"] as! Int
                        
                        let goalsAgainst = standingsFromJson["goalsAgainst"] as! Int
                        
                        let goalDifference = standingsFromJson["goalDifference"] as! Int
                        
                        let wins = standingsFromJson["wins"] as! Int
                        
                        let draws = standingsFromJson["draws"] as! Int
                        
                        let losses = standingsFromJson["losses"] as! Int
                        
                        self.standings.append(LeagueTable(position: position, teamName: teamName, playedGames: playedGames, points: points, goals: goals, goalsAgainst: goalsAgainst, goalDifference: goalDifference, wins: wins, draws: draws, losses: losses))
                        
                        print(position,teamName,playedGames,points,goals,goalsAgainst,goalDifference,wins,draws,losses)
           
                        
                        self.standingTableView.reloadData()
                        
                        
                    }
                }
            }
            
            
            
        }
        if teamsLeagueCode == "444"
        {
            isBra = true
        }
        else if teamsLeagueCode == "449"
        {
            isNet = true
        }
        else if teamsLeagueCode == "445"
        {
            isPrem = true
        }
        else if teamsLeagueCode == "450"
        {
            isFre = true
        }
        else if teamsLeagueCode == "452"
        {
            isGer = true
        }
        else
        {
        }
        
        label1.backgroundColor = UIColor(red: 110/255, green: 200/255, blue: 92/255, alpha: 1)
        label2.backgroundColor = UIColor(red: 95/255, green: 161/255, blue: 92/255, alpha: 1)
        label3.backgroundColor = UIColor(red: 97/255, green: 159/255, blue: 196/255, alpha: 1)
        label4.backgroundColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 1)
        
        label1.layer.masksToBounds = true
        label2.layer.masksToBounds = true
        label3.layer.masksToBounds = true
        label4.layer.masksToBounds = true
        label1.layer.cornerRadius = 9
        label2.layer.cornerRadius = 9
        label3.layer.cornerRadius = 9
        label4.layer.cornerRadius = 9
        
        

        // Do any additional setup after loading the view.
    
        standingTableView.register(UINib(nibName: "StandingTableCell",bundle: nil), forCellReuseIdentifier: "StandingTableCell")
        standingTableView.allowsSelection = false
        standingTableView.separatorStyle = .none
        view.addSubview(standingTableView)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = standingTableView.dequeueReusableCell(withIdentifier: "StandingTableCell", for: indexPath) as! StandingTableCell
        
        cell.positionLabel.text = String(standings[indexPath.row].getPosition()) + "."
        cell.teamNameLabel.text = standings[indexPath.row].getTeamName()
        cell.playedGamesLabel.text = String(standings[indexPath.row].getPlayedGames())
        cell.pointsLabel.text = String(standings[indexPath.row].getPoints())
        cell.goalsLabel.text = String(standings[indexPath.row].getGoals())
        cell.goalsAgainstLabel.text = String(standings[indexPath.row].getGoalsAgainst())
        cell.goalDifferenceLabel.text = String(standings[indexPath.row].getGoalDifference())
        cell.winLabel.text = String(standings[indexPath.row].getWins())
        cell.drawLabel.text = String(standings[indexPath.row].getDraws())
        cell.losesLabel.text = String(standings[indexPath.row].getLosses())
        
        
        
        
        cell.positionLabel.backgroundColor = cellColor(position: standings[indexPath.row].getPosition())
        
        if indexPath.row % 2 == 0
        {
            cell.contentView.backgroundColor = UIColor(red: 220/255, green: 200/255, blue: 220/255, alpha: 1)
            
        }
        else
        {
            cell.contentView.backgroundColor  = UIColor (red: 220/255, green: 222/255, blue: 226/255, alpha: 1)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return standings.count
    }
    
    
    func cellColor(position: Int) -> UIColor
    {
        
        if isBra
        {
            label1.text = "Champ. & C.Libertadores"
            label2.text = "Copa Libertadores"
            label3.text = "Copa Sudamericana"
            label4.text = "Relegated"
            
            
            switch position {
            case 1:
                cellColor = UIColor(red: 110/255, green: 200/255, blue: 92/255, alpha: 1)
            case 2,3,4,5:
                cellColor = UIColor(red: 95/255, green: 161/255, blue: 92/255, alpha: 1)
            case 6,7,8,9,10,11,12:
                cellColor = UIColor(red: 97/255, green: 159/255, blue: 196/255, alpha: 1)
            case 17,18,19,20:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 1)
            default:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 0)            }
        }
        
        if isPrem
        {
            label1.text = "Champ. & Champ.League"
            label2.text = "Champions League"
            label3.text = "UEFA Europa League"
            label4.text = "Relegated"
            
            
            switch position {
            case 1:
                cellColor = UIColor(red: 110/255, green: 200/255, blue: 92/255, alpha: 1)
            case 2,3,4:
                cellColor = UIColor(red: 95/255, green: 161/255, blue: 92/255, alpha: 1)
            case 5,6,7:
                cellColor = UIColor(red: 97/255, green: 159/255, blue: 196/255, alpha: 1)
            case 18,19,20:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 1)
            default:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 0)
            }
            
        }
        
        if isFre
        {
            label1.text = "Champ. & Champ.League"
            label2.text = "Champions League"
            label3.text = "UEFA Europa League"
            label4.text = "Relegated"
            
            
            switch position {
            case 1:
                cellColor = UIColor(red: 110/255, green: 200/255, blue: 92/255, alpha: 1)
            case 2,3:
                cellColor = UIColor(red: 95/255, green: 161/255, blue: 92/255, alpha: 1)
            case 4,5,6:
                cellColor = UIColor(red: 97/255, green: 159/255, blue: 196/255, alpha: 1)
            case 18,19,20:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 1)
            default:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 0)
            }
            
        }
        
        if isNet
        {
            label1.text = "Champ. & Champ.League"
            label2.text = "Champions League"
            label3.text = "UEFA Europa League"
            label4.text = "Relegated"
            
            
            switch position {
            case 1:
                cellColor = UIColor(red: 110/255, green: 200/255, blue: 92/255, alpha: 1)
            case 2:
                cellColor = UIColor(red: 95/255, green: 161/255, blue: 92/255, alpha: 1)
            case 3,4,5,6,7:
                cellColor = UIColor(red: 97/255, green: 159/255, blue: 196/255, alpha: 1)
            case 18,19,20:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 1)
            default:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 0)
            }
        }
        
        if isGer
        {
            label1.text = "Champ. & Champ.League"
            label2.text = "Champions League"
            label3.text = "UEFA Europa League"
            label4.text = "Relegated"
            
            
            switch position {
            case 1:
                cellColor = UIColor(red: 110/255, green: 200/255, blue: 92/255, alpha: 1)
            case 2,3,4:
                cellColor = UIColor(red: 95/255, green: 161/255, blue: 92/255, alpha: 1)
            case 5,6:
                cellColor = UIColor(red: 97/255, green: 159/255, blue: 196/255, alpha: 1)
            case 16,17,18:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 1)
            default:
                cellColor = UIColor(red: 255/255, green: 48/255, blue: 60/255, alpha: 0)
            }
        }

        
        return cellColor
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
