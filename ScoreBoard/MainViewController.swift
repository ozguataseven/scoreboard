//
//  MainViewController.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 27/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toLiveScores" {
            
        }
        else {
            let data = segue.destination as! OptionsiewController
        
            if segue.identifier == "franceToOptions"{
                data.leagueCode = "450"
            }
            else if segue.identifier == "englandToOptions"{
                data.leagueCode = "445"
            }
            else if segue.identifier == "netherlandToOptions"{
                data.leagueCode = "449"
            }
            else if segue.identifier == "brasilToOptions"{
                data.leagueCode = "444"
            }
            else if segue.identifier == "germanyToOptions"{
                data.leagueCode = "452"
            }
            
            else{}
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
