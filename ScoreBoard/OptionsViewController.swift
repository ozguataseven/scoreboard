//
//  MainViewController.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 26/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit
import Alamofire

class OptionsiewController: UIViewController {
    
    
    var leagueCode = String()
    
    var currentMatchDay = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        Alamofire.request("http://api.football-data.org/v1/competitions").responseJSON { (response) in
            if let JSON = response.result.value as? [[String : AnyObject]]{
                
                print(JSON)
                
                
                    for JSON in JSON{
                        
                        
                        let leagueID = JSON["id"] as! Int
                        let matchDay = JSON["currentMatchday"] as! Int
                        
                        if leagueID == Int(self.leagueCode)
                        {
                            self.currentMatchDay = matchDay
                        }
                
                        
                        print(leagueID,matchDay)
                        
                    
                }
            }
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTeamView"{
            let data = segue.destination as! TeamsViewController
        
            data.teamsLeagueCode = leagueCode
        }
        if segue.identifier == "toLeagueTablesView"{
            let data = segue.destination as! StandingViewController
            
            data.teamsLeagueCode = leagueCode
        }
        
        if segue.identifier == "toFixturesView"{
            let data = segue.destination as! TeamFixtureViewController
            
            data.teamsLeagueCode = leagueCode
            data.currentMatchDay = currentMatchDay
            print(data.teamsLeagueCode)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
