//
//  Players.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 28/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import Foundation

class Players{
    private var name : String?
    private var position : String?
    private var jerseyNumber : Int?
    private var dateOfBirth : String?
    private var nationality : String?
    private var contractUntil : String?

    public func getName()->String{
        return name!
    }
    public func getPosition()->String{
        return position!
    }
    public func getJerseyNumber()->Int{
        return jerseyNumber!
    }
    public func getDateOfBirth()->String{
        return dateOfBirth!
    }
    public func getNationality()->String{
        return nationality!
    }
    public func getContractUntil()->String{
        return contractUntil!
    }
    
    init(name: String, position: String, jerseyNumber: Int, dateOfBirth: String ,nationality: String, contractUntil: String) {
       
        self.name = name
        self.position = position
        self.jerseyNumber = jerseyNumber
        self.dateOfBirth = dateOfBirth
        self.nationality = nationality
        self.contractUntil = contractUntil
    
    }

    
}
