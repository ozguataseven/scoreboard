//
//  ScoreTableCellTableViewCell.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 24/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit

class ScoreTableCellTableViewCell: UITableViewCell {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
