//
//  FixturesResultObject.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 23/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit

class FixturesResultObject : NSObject{

    var goalsHomeTeam : String = ""
    var goalsAwayTeam : String = ""
    
}
