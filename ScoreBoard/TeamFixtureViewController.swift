//
//  TeamFixtureViewController.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 30/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit
import Alamofire

class TeamFixtureViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource {

    var teamsLeagueCode = String()
    
    var teamFixtures = [Fixtures]()
    
    var listedTeamFixtures = [Fixtures]()

    @IBOutlet var leftButton: UIButton!
    
    @IBOutlet var rightButton: UIButton!
    
    @IBOutlet var teamFixtureTableView: UITableView!
    
    @IBOutlet var weekOfFixture: UILabel!
    
    var currentMatchDay = Int()
    
    var fixtureAvailable : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentMatchDay = currentMatchDay - 1
        
        if currentMatchDay == 0 {
            
            weekOfFixture.text = "Fixture not ready"

            fixtureAvailable = false
        }
        else
        {
            weekOfFixture.text = "Week " + String(currentMatchDay)
            
            fixtureAvailable = true
        }
        
        alamofireRequest()
        
        
        leftButton.layer.masksToBounds = true
        leftButton.layer.cornerRadius = 15
        rightButton.layer.masksToBounds = true
        rightButton.layer.cornerRadius = 15
        
        leftButton.addTarget(self, action: #selector(self.LeftTap), for: UIControlEvents.touchUpInside)
        
        rightButton .addTarget(self, action: #selector(self.RightTap), for: UIControlEvents.touchUpInside)
      
        teamFixtureTableView.register(UINib(nibName: "ScoreTableCell",bundle: nil), forCellReuseIdentifier: "ScoreTableCell")
        teamFixtureTableView.delegate = self
        teamFixtureTableView.dataSource = self
        teamFixtureTableView.allowsSelection = false
        teamFixtureTableView.separatorStyle = .none
        teamFixtureTableView.backgroundColor = UIColor(red: 81/255, green: 104/255, blue: 130/255, alpha: 1)
        view.addSubview(teamFixtureTableView)
        view.backgroundColor = UIColor(red: 81/255, green: 104/255, blue: 130/255, alpha: 1)
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = teamFixtureTableView.dequeueReusableCell(withIdentifier: "ScoreTableCell", for: indexPath) as! ScoreTableCell
        cell.homeTeamLabel.text = teamFixtures[indexPath.row].getHomeTeamName()
        cell.awayTeamLabel.text = teamFixtures[indexPath.row].getAwayTeamName()
        cell.score.text = teamFixtures[indexPath.row].getScore()
        cell.xibView.backgroundColor = UIColor(red: 81/255, green: 104/255, blue: 130/255, alpha: 1)
        
        if indexPath.row % 2 == 0
        {
            cell.contentView.backgroundColor = UIColor(red: 220/255, green: 200/255, blue: 220/255, alpha: 1)
            
        }
        else
        {
            cell.contentView.backgroundColor  = UIColor (red: 220/255, green: 222/255, blue: 226/255, alpha: 1)
        }
        
        return cell

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamFixtures.count
    }
    
    func alamofireRequest()
    {
        if currentMatchDay <= 0 {
            
            weekOfFixture.text = "Fixture not ready"
            
            currentMatchDay = 0
            
            self.teamFixtureTableView.reloadData()
        }
        
        else {
        Alamofire.request("http://api.football-data.org/v1/competitions/" + teamsLeagueCode + "/fixtures").responseJSON { (response) in
            if let JSON = response.result.value as? [String : Any]{
                
                if let fixturesFromJson = JSON["fixtures"] as? [[String : AnyObject]] {
                    
                    for fixturesFromJson in fixturesFromJson{
                        
                        
                        let homeTeamName = fixturesFromJson["homeTeamName"] as! String
                        let awayTeamName = fixturesFromJson["awayTeamName"] as! String
                        let matchDay = fixturesFromJson["matchday"] as! Int
                        
                        let result = fixturesFromJson["result"] as? [String : AnyObject]
                        
                        var score = String()
                        
                        
                        if let goalsHomeTeam = result?["goalsHomeTeam"] as? Int{
                            if let goalsAwayTeam = result?["goalsAwayTeam"] as? Int
                            {
                                score = String(goalsHomeTeam) + " - " + String(goalsAwayTeam)
                            }
                            
                        }
                        else
                        {
                            score = "VS"
                        }
                        
                        
                        
                        
                        if matchDay == self.currentMatchDay {
                            
                            
                            
                        
                            self.teamFixtures.append(Fixtures(home: homeTeamName, away: awayTeamName, score: score ,matchDay: matchDay))
                        
                            print(score)
                        
                        
                        
                        
                            self.teamFixtureTableView.reloadData()
                        }
                        
                        
                    }
                }
            }
            
            }
        }
    }
    
    func LeftTap()
    {
        if fixtureAvailable{
        
            teamFixtures.removeAll()
            currentMatchDay = currentMatchDay - 1
            weekOfFixture.text = "Week " + String(currentMatchDay)
            alamofireRequest()
            
        }
     
    }
    func RightTap()
    {
        
        if fixtureAvailable{
            
            teamFixtures.removeAll()
            currentMatchDay = currentMatchDay + 1
            weekOfFixture.text = "Week " + String(currentMatchDay)
            alamofireRequest()
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
