//
//  StandingTableCell.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 29/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit

class StandingTableCell: UITableViewCell {

    @IBOutlet var positionLabel: UILabel!
    
    @IBOutlet var teamNameLabel: UILabel!
    
    @IBOutlet var playedGamesLabel: UILabel!
    
    @IBOutlet var winLabel: UILabel!
    
    @IBOutlet var drawLabel: UILabel!
    
    @IBOutlet var losesLabel: UILabel!
    
    @IBOutlet var goalsLabel: UILabel!
    
    @IBOutlet var goalsAgainstLabel: UILabel!
    
    @IBOutlet var goalDifferenceLabel: UILabel!
    
    @IBOutlet var pointsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
