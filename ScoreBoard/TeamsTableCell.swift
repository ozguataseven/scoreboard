//
//  TeamsTableCell.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 27/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit

class TeamsTableCell: UITableViewCell {

    @IBOutlet var teamName: UILabel!
    
    @IBOutlet var teamInfoBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
