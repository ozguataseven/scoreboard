//
//  PlayerTableCell.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 28/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit

class PlayerTableCell: UITableViewCell {

    @IBOutlet var playerProfile: UIImageView!
    @IBOutlet var playerPosition: UIImageView!
    @IBOutlet var playerLabel: UILabel!
    @IBOutlet var dateOfBirthLabel: UILabel!
    @IBOutlet var kitNo: UILabel!
    @IBOutlet var nationalityLabel: UILabel!
    @IBOutlet var contractUntilLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
