//
//  ScoreTableCell.swift
//  ScoreBoard
//
//  Created by Ozgu Ataseven on 24/06/2017.
//  Copyright © 2017 Ozgu Ataseven. All rights reserved.
//

import UIKit

class ScoreTableCell: UITableViewCell{
    
    @IBOutlet var xibView: UIView!

    @IBOutlet var homeTeamLabel: UILabel!
    
    @IBOutlet var awayTeamLabel: UILabel!
    
    @IBOutlet var score: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
